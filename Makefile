.PHONY: build
build: build-css build-go

build-css:
	npx tailwindcss-cli@latest build resources/css/*.css -o static/css/core.css

build-go:
	mkdir -p ./bin
	go build -o ./bin ./cmd/blog
