package models

import (
	"html/template"
	"time"

	"gorm.io/gorm"
)

// A Post is used to represent a blog post's content and metadata
type Post struct {
	gorm.Model
	ID           int       `gorm:"primaryKey"`
	CreatedAt    time.Time `gorm:"autoCreateTime"`
	Author       Author
	AuthorID     int
	Title        string
	Abstract     string
	Cover        string // url
	CoverAlt     string
	CoverCredits string
	Content      template.HTML
}
