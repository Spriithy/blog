package models

import (
	"os"

	"gitlab.com/Spriithy/blog/blog/utils"
	"gorm.io/gorm"
)

// AnonymousAuthor might be used when publishing an article anonymously
// or when a post's author has been removed from the database
var AnonymousAuthor = Author{ID: 0, FullName: "Anonyme", Meta: "On ne sait pas trop"}

// An Author is a post writer
type Author struct {
	gorm.Model
	ID         int `gorm:"primaryKey"`
	FullName   string
	Meta       string
	Avatar     string // URL
	Username   string
	BcryptPass string
}

func (author *Author) InitPassword(password string) {
	if author.BcryptPass == "" {
		authorPassword, err := utils.HashPassword(password)
		if err != nil {
			return
		}

		author.BcryptPass = authorPassword
	}
}

func (author *Author) ChangePassword(oldPassword, newPassword string) bool {
	if utils.CheckPassword(oldPassword, author.BcryptPass) {
		authorPassword, err := utils.HashPassword(newPassword)
		if err != nil {
			return false
		}

		author.BcryptPass = authorPassword
		return true
	}

	return false
}

// DefaultAvatar helps ensure that an avatar is always defined for the author
func (Author) DefaultAvatar() string {
	return os.Getenv("DEFAULT_AUTHOR_AVATAR")
}
