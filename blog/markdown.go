package blog

import (
	"html/template"
	"io"
	"io/ioutil"

	"github.com/alecthomas/chroma"
	"github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	mdhtml "github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
)

// The ChromaHTMLRenderer is a custom renderer that parses Markdown CodeBlocks
// using Chroma and produces colorful syntax highlighting.
type ChromaHTMLRenderer struct {
	r *mdhtml.Renderer
}

func newChromaHTMLRenderer(r *mdhtml.Renderer) *ChromaHTMLRenderer {
	return &ChromaHTMLRenderer{
		r: r,
	}
}

// RenderHeader writes HTML document preamble and TOC if requested.
func (c *ChromaHTMLRenderer) RenderHeader(w io.Writer, ast ast.Node) {
	c.r.RenderHeader(w, ast)
}

// RenderNode renders a markdown node to HTML
// This specific method handled *ast.CodeBlock nodes and renders them with Chroma-parsed HTML.
func (c *ChromaHTMLRenderer) RenderNode(w io.Writer, node ast.Node, entering bool) ast.WalkStatus {
	switch codeBlock := node.(type) {
	case *ast.CodeBlock:
		return c.renderCodeBlock(w, codeBlock)
	}
	return c.r.RenderNode(w, node, entering)
}

func (c *ChromaHTMLRenderer) renderCodeBlock(w io.Writer, codeBlock *ast.CodeBlock) ast.WalkStatus {
	var (
		lang      string
		lexer     chroma.Lexer
		formatter *html.Formatter
	)

	rawCode := string(codeBlock.Literal)

	// First try to detect if the user defined the language
	if lang = string(codeBlock.Info); len(lang) > 0 {
		lexer = lexers.Get(lang)
	} else {
		// If not, try to make chroma guess it
		lexer = lexers.Analyse(rawCode)
	}

	// No lexer could be found, fallback to default one
	if lexer == nil {
		lexer = lexers.Fallback
	}

	// Parse and format the HTML output
	formatter = html.New(html.TabWidth(4), html.WithClasses(true), html.ClassPrefix("markdown-code-"))
	tokens, _ := lexer.Tokenise(nil, string(rawCode))
	formatter.Format(w, styles.BlackWhite, tokens)
	return ast.SkipChildren
}

// RenderFooter writes HTML document footer.
func (c *ChromaHTMLRenderer) RenderFooter(w io.Writer, ast ast.Node) {
	c.r.RenderFooter(w, ast)
}

// Store cached markdown data
var markdownCache map[string]template.HTML = map[string]template.HTML{}

// ParseMarkdown leverages cache storage and gomarkdown/markdown to provide
// Markdown content as HTML.
func ParseMarkdown(path string) template.HTML {
	// Check if requested path is cached
	if parsed, _ := markdownCache[path]; parsed != "" {
		return parsed
	}

	// Setup the parser
	parserFlags := parser.CommonExtensions | parser.Footnotes | parser.HeadingIDs | parser.AutoHeadingIDs
	parser := parser.NewWithExtensions(parserFlags)

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return ""
	}

	// Parse the Markdown file's content using the ChromaHTMLRenderer
	opts := mdhtml.RendererOptions{Flags: mdhtml.CommonFlags}
	renderer := newChromaHTMLRenderer(mdhtml.NewRenderer(opts))
	html := markdown.Render(parser.Parse(data), renderer)

	// Update the cache entry
	markdownCache[path] = template.HTML(html)

	return markdownCache[path]
}
