package blog

import (
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/Spriithy/blog/blog/models"
)

func (blog *Blog) serveDashboard(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "dashboard.html", gin.H{
		"title":     "Tableau de bord",
		"activeTab": "dashboard",
	})
}

func (blog *Blog) serveProfile(ctx *gin.Context) {
	user := blog.userFunc(ctx)
	ctx.HTML(http.StatusOK, "profile.html", gin.H{
		"title":     "Mon profil",
		"activeTab": "profile",
		"user":      user,
	})
}

func profileError(ctx *gin.Context, code int, user *models.Author, title, body string) {
	ctx.HTML(code, "profile.html", gin.H{
		"title":     "Mon profil",
		"activeTab": "profile",
		"user":      user,
		"banner": Banner{
			Type:  "error",
			Title: title,
			Body:  body,
		},
	})
}

func (blog *Blog) postProfile(ctx *gin.Context) {
	user := blog.userFunc(ctx)

	// Update user's full name if needed
	if fullName, set := ctx.GetPostForm("full_name"); true {
		if len(fullName) == 0 || !set {
			profileError(ctx, http.StatusBadRequest, user, "Veuillez saisir un nom complet valide", "")
			return
		}

		user.FullName = fullName
	}

	// Update user's meta description
	if meta, ok := ctx.GetPostForm("meta_desc"); ok {
		user.Meta = meta
	}

	// Try and process avatar upload
	fileHeader, err := ctx.FormFile("avatar")
	if err != nil && err != http.ErrMissingFile {
		profileError(ctx, http.StatusInternalServerError, user, "Une erreur est survenue", "Une erreur est survenue lors du téléversement de votre avatar")
		return
	}

	// If an image was uploaded...
	if fileHeader != nil {
		// Check file size is <= 500 Ko
		if fileHeader.Size/1024 > 500 {
			profileError(ctx, http.StatusRequestEntityTooLarge, user, "Image trop volumineuse", "Merci d'utiliser une image dont le poids est inférieur à 500 Ko")
			return
		}

		// Try to open uploaded file
		file, err := fileHeader.Open()
		defer file.Close()
		if err != nil {
			profileError(ctx, http.StatusInternalServerError, user, "Une erreur est survenue", "Une erreur est survenue lors du téléversement de votre avatar")
			return
		}

		// Read from uploaded file
		bytes, err := ioutil.ReadAll(file)
		if err != nil {
			profileError(ctx, http.StatusInternalServerError, user, "Une erreur est survenue", "Une erreur est survenue lors du téléversement de votre avatar")
			return
		}

		// Save new image
		fileExt := filepath.Ext(fileHeader.Filename)
		fileName := strings.ReplaceAll(user.Username, " ", "_")
		filePath := strings.ToLower("static/u/avatars/" + fileName + fileExt)
		if _, err := os.Stat(filePath); err == nil || !os.IsNotExist(err) {
			os.Remove(filePath)
		}
		ioutil.WriteFile(filePath, bytes, 0644)

		// Set user's avatar URL
		user.Avatar = blog.asset(filePath)
	}

	// Finally, update the user's data
	blog.db.Save(user)

	ctx.HTML(http.StatusOK, "profile.html", gin.H{
		"title":     "Mon profil",
		"activeTab": "profile",
		"user":      user,
		"banner": Banner{
			Type:  "success",
			Title: "Modifications réussies",
		},
	})
}
