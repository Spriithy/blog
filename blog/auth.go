package blog

import (
	"net/http"
	"strings"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	jwt3 "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/Spriithy/blog/blog/models"
	"gitlab.com/Spriithy/blog/blog/utils"
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func translateJWTError(message string) string {
	switch message {
	case jwt.ErrFailedAuthentication.Error():
		return "Mot de passe ou nom d'utilisateur incorrect"
	case jwt.ErrMissingLoginValues.Error():
		return "Nom d'utilisateur ou mot de passe manquant"
	case jwt.ErrFailedTokenCreation.Error():
		return "Erreur lors de la création du jeton"
	case jwt.ErrForbidden.Error():
		return "Vous n'avez pas l'autorisation d'accéder à cette ressource"
	case jwt.ErrEmptyCookieToken.Error():
		return "Vous n'êtes pas connecté"
	}
	return "Erreur inconnue"
}

func (blog *Blog) AuthMiddleware(realm, identityKey string, jwtSecretKey []byte) (*jwt.GinJWTMiddleware, error) {
	blog.userFunc = func(c *gin.Context) *models.Author {
		var author models.Author
		claims := jwt.ExtractClaims(c)
		username := claims[identityKey].(string)
		blog.db.Where("username = ?", username).First(&author)
		return &author
	}
	return jwt.New(&jwt.GinJWTMiddleware{
		Realm:       realm,
		Key:         jwtSecretKey,
		Timeout:     time.Hour,
		MaxRefresh:  0,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			claims := jwt.MapClaims{}
			if author, ok := data.(models.Author); ok {
				claims[identityKey] = author.Username
			}
			return claims
		},
		IdentityHandler: func(ctx *gin.Context) interface{} {
			return *blog.userFunc(ctx)
		},
		Authenticator: func(ctx *gin.Context) (interface{}, error) {
			var (
				attempt login
				author  models.Author
			)

			if err := ctx.ShouldBind(&attempt); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			blog.db.Where("username = ?", attempt.Username).First(&author)

			if utils.CheckPassword(attempt.Password, author.BcryptPass) {
				return author, nil
			}

			// Failed login attempt
			return nil, jwt.ErrFailedAuthentication
		},
		Unauthorized: func(ctx *gin.Context, code int, message string) {
			username := ctx.PostForm("username")

			// Manually extract claims
			cookie, _ := ctx.Request.Cookie("token")

			// Chech session is not expired
			_, err := jwt3.Parse(cookie.Value, func(t *jwt3.Token) (interface{}, error) { return jwtSecretKey, nil })
			expired := strings.ToLower(err.Error()) == strings.ToLower(jwt.ErrExpiredToken.Error())
			if expired {
				ctx.HTML(http.StatusUnauthorized, "login.html", LoginPage{
					Username: username,
					Banner: Banner{
						Type:  "warning",
						Title: "Votre session a expiré",
						Body:  "Merci de bien vouloir vous reconnecter",
					},
				})
				return
			}

			switch ctx.FullPath() {
			case "/login":
				ctx.HTML(http.StatusOK, "login.html", LoginPage{
					Username: username,
					Error:    translateJWTError(message),
				})
			default:
				ctx.HTML(http.StatusUnauthorized, "login.html", LoginPage{
					Username: username,
					Banner: Banner{
						Type:  "error",
						Title: "Accès refusé",
						Body:  "Vous n'avez pas accès à cette ressource",
					},
				})
			}
		},
		LoginResponse: func(ctx *gin.Context, code int, token string, expire time.Time) {
			ctx.Redirect(http.StatusFound, "/a/dashboard")
		},
		SendCookie:     true,
		SecureCookie:   true,
		CookieHTTPOnly: true,
		CookieDomain:   blog.ServerName,
		CookieName:     "token",
		CookieMaxAge:   time.Hour * 24 * 31, // Allow Cookie to stay in browser to check whether token is expired
		TokenLookup:    "cookie:token",
		TimeFunc:       time.Now,
	})
}

func (blog *Blog) serveLogin(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "login.html", LoginPage{})
}
