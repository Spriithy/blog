package blog

import (
	"html/template"

	"gitlab.com/Spriithy/blog/blog/models"
)

var (
	shortLoremIpsum = "Accusantium ullam, dicta explicabo error delectus consequuntur"
	loremIpsum      = template.HTML("Two engineers at Salesforce talk about how they decoupled a complex library from old spaghetti logic, then open sourced that library by creating a new internal process where none existed before.")
)

type Page interface {
	Title() string
}

func BasePage(title string) Page {
	return basePage{title: title}
}

type basePage struct {
	title string
}

func (p basePage) Title() string {
	return p.title
}

type BlogPage struct{ Posts []models.Post }

func (BlogPage) Title() string {
	return "Blog"
}

type PostPage struct {
	models.Post
}

func (p PostPage) Title() string {
	return p.Post.Title
}

type LoginPage struct {
	Username string
	Error    string
	Banner   Banner
}

type Banner struct {
	Type  string
	Title string
	Body  string
}

func (b Banner) Color() string {
	switch b.Type {
	case "error":
		return "red"
	case "warning":
		return "yellow"
	case "info":
		return "blue"
	case "success":
		return "green"
	default:
		return "yellow"
	}
}

func (LoginPage) Title() string {
	return "Connexion"
}
