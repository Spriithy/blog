package blog

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/goodsign/monday"
	"gitlab.com/Spriithy/blog/blog/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var templateDirs = []string{"templates/**/*.html", "templates/*.html"}

type BlogDB struct {
	Host     string
	Port     int
	Database string
	Username string
	Password string
}
type Blog struct {
	Scheme     string
	Host       string
	Port       int
	ServerName string
	DB         BlogDB
	db         *gorm.DB
	userFunc   func(*gin.Context) *models.Author
}

// Serve allows to start the server and accept HTTP connections and requests
func (blog *Blog) Serve() {
	// Get JWT secret key
	jwtSecret := os.Getenv("JWT_SECRET")
	if jwtSecret == "" {
		jwtSecret = "default JWT secret"
	}

	// Default host is 0.0.0.0
	if blog.Host == "" {
		blog.Host = "0.0.0.0"
	}

	// Default scheme is http
	if blog.Scheme == "" {
		blog.Scheme = "http"
	}

	// Default port is 8000
	if blog.Port == 0 {
		blog.Port = 8000
	}

	authMiddleware, err := blog.AuthMiddleware("blog", "id", []byte(jwtSecret))
	if err != nil {
		log.Fatal("JWT Error: " + err.Error())
	}

	err = authMiddleware.MiddlewareInit()
	if err != nil {
		log.Fatal("authMiddleware.MiddlewareInit() Error:" + err.Error())
	}

	// Configure routes
	router := gin.Default()
	router.GET("/", blog.serveBlog)
	router.GET("/posts/:id", blog.servePost)
	router.Static("/static", "static")

	// Admin routes
	router.GET("/login", blog.serveLogin)
	router.POST("/login", authMiddleware.LoginHandler)

	admin := router.Group("/a/dashboard")
	admin.Use(authMiddleware.MiddlewareFunc())
	admin.GET("/", blog.serveDashboard)
	admin.GET("/profile", blog.serveProfile)
	admin.POST("/profile", blog.postProfile)

	// Setup templates
	var templates []string
	router.SetFuncMap(blog.funcMap())
	for _, templateDir := range templateDirs {
		globFiles, _ := filepath.Glob(templateDir)
		templates = append(templates, globFiles...)
	}
	router.LoadHTMLFiles(templates...)

	router.NoRoute(blog.serve404)

	// Get local timezome
	timezone, _ := time.Now().Local().Zone()
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s database=%s sslmode=disable TimeZone=%s", blog.DB.Host, blog.DB.Port, blog.DB.Username, blog.DB.Password, blog.DB.Database, timezone)
	blog.db, _ = gorm.Open(postgres.New(postgres.Config{DSN: dsn}), &gorm.Config{})

	// Create tables
	blog.db.AutoMigrate(&models.Author{}, &models.Post{})
	blog.db.Clauses(clause.OnConflict{DoNothing: true}).Create(&models.AnonymousAuthor)

	/*
		spriithy := models.Author{
			Username: "Spriithy",
			FullName: "Théophile Dano",
			Meta:     "Développeur",
		}
		spriithy.InitPassword("foobarbaz")

		blog.db.Create(&spriithy)
	*/

	// Start server
	switch strings.ToLower(blog.Scheme) {
	case "http", "https":
		router.Run(blog.Host + ":" + fmt.Sprint(blog.Port))
	default:
		log.Fatalf("Unsupported scheme: %s\n", blog.Scheme)
	}
}

func (blog *Blog) funcMap() template.FuncMap {
	return template.FuncMap{
		"link":     blog.path,
		"asset":    blog.asset,
		"strftime": blog.strftime,
		"dict": func(values ...interface{}) (map[string]interface{}, error) {
			// https://stackoverflow.com/questions/18276173/calling-a-template-with-several-pipeline-parameters
			if len(values)%2 != 0 {
				return nil, errors.New("invalid dict call")
			}
			dict := make(map[string]interface{}, len(values)/2)
			for i := 0; i < len(values); i += 2 {
				key, ok := values[i].(string)
				if !ok {
					return nil, errors.New("dict keys must be strings")
				}
				dict[key] = values[i+1]
			}
			return dict, nil
		},
	}
}

func (blog *Blog) hostname(blindPort bool) string {
	host := blog.Host
	if blog.ServerName != "" {
		host = blog.ServerName
	}

	if !blindPort && blog.Port != 80 && blog.Port != 443 {
		return fmt.Sprintf("%s://%s:%d", blog.Scheme, host, blog.Port)
	}

	return fmt.Sprintf("%s://%s", blog.Scheme, host)
}

func (blog *Blog) path(path ...interface{}) string {
	var fullPath bytes.Buffer

	stringPart := func(val string) {
		val = strings.Trim(val, "/")
		if len(val) > 0 {
			fullPath.WriteByte('/')
			fullPath.WriteString(val)
		}
	}

	intPart := func(val int) {
		stringPart(strconv.Itoa(val))
	}

	for _, pathPart := range path {
		switch part := pathPart.(type) {
		case string:
			stringPart(part)
		case int:
			intPart(part)
		}
	}
	return fmt.Sprintf("%s%s", blog.hostname(true), fullPath.String())
}

func (blog *Blog) asset(path string) string {
	path = strings.TrimPrefix(path, "static")
	path = strings.TrimPrefix(path, "/static")
	return blog.path("static", path)
}

func (blog *Blog) strftime(date time.Time) string {
	formattedDate := monday.Format(date, monday.DefaultFormatFrFRFull, monday.LocaleFrFR)
	return strings.Title(formattedDate)
}

func (blog *Blog) serve404(ctx *gin.Context) {
	ctx.HTML(http.StatusNotFound, "error.html", gin.H{
		"code":    404,
		"title":   "Page introuvable",
		"message": template.HTML("La page demandée n'a pas pu être trouvée.<br>Vérifiez que l'adresse de la page soit correcte ou que la page n'a pas été déplacée."),
	})
}

func (blog *Blog) serveBlog(ctx *gin.Context) {
	var posts []models.Post
	blog.db.Preload("Author").Limit(7).Order("created_at desc").Find(&posts)
	ctx.HTML(http.StatusOK, "blog.html", BlogPage{posts})
}

func (blog *Blog) servePost(ctx *gin.Context) {
	var post models.Post

	id := ctx.Param("id")
	blog.db.First(&post, id)

	post.Content = ParseMarkdown("resources/markdown/test.md")
	ctx.HTML(http.StatusOK, "post.html", post)
}
