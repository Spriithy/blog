package main

import (
	"os"
	"strconv"

	"github.com/joho/godotenv"
	"gitlab.com/Spriithy/blog/blog"
)

func main() {
	godotenv.Load()

	dbHost := os.Getenv("DB_HOST")
	dbPort, err := strconv.Atoi(os.Getenv("DB_PORT"))
	if err != nil {
		dbPort = 5432
	}
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbDatabase := os.Getenv("DB_DATABASE")

	blog := blog.Blog{
		Scheme: "https",
		Port:   8443, Host: "127.0.0.1",
		ServerName: "blog.dev-lab.net",
		DB: blog.BlogDB{
			Host:     dbHost,
			Port:     dbPort,
			Username: dbUser,
			Password: dbPass,
			Database: dbDatabase,
		},
	}
	blog.Serve()
}
