# This is a sample Markdown document

It comprises some useful testing content.

#### Now with some code

```c++
int main(int argc, char **argv) {
    return 0;
}
```

And also a random shopping list...

+ Carrots
+ Onions
+ Beef
+ Tissues
+ ...

---------------

## I want to test blockquotes as well

Before I do such a thing, I want to highlight some inline code as well `such as this`. I will now let Churchill speak :

> This is a quote from Churchill himself.
>
> -- **Churchill**

###### A quick note on sources

It is important to source your articles. This is why I'm including the [source of the preceding quote from Churchill](https://google.com/). It is sometimes useful to include sources as footnotes[^1] rather than inline[^longnote].

1. Write the press release
2. Update the website
3. Contact the media

### Some definitions


[^1]: The sources for this assertion could not be found.
[^longnote]: Likewise, the sources for this information are nowhere to be found.
