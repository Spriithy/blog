module gitlab.com/Spriithy/blog

go 1.15

require (
	github.com/alecthomas/chroma v0.8.2
	github.com/appleboy/gin-jwt/v2 v2.6.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/gomarkdown/markdown v0.0.0-20201113031856-722100d81a8e
	github.com/goodsign/monday v1.0.0
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/ugorji/go v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392
	golang.org/x/sys v0.0.0-20201126233918-771906719818 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gorm.io/driver/postgres v1.0.5
	gorm.io/gorm v1.20.7
)
