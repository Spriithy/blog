// tailwind.config.js

const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    // enabled: true,
    content:[
      './templates/**/*.html',
      './templates/*.html',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['-apple-system','BlinkMacSystemFont','Segoe UI','Helvetica','Arial','sans-serif','Apple Color Emoji','Segoe UI Emoji'],
        serif: ['Bitter', ...defaultTheme.fontFamily.serif],
      },
      colors: {
        orange: colors.orange,
        ...defaultTheme.colors,
      },
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/forms')
  ],
}
